"""
Methods and Classes definition related to view

Author: @Ji_Qi
"""
import ipyleaflet as ill
import math
from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
import folium
from folium.plugins import FloatImage
import pathlib
import cv2
from pyproj import Proj, transform
import pandas as pd
import webbrowser
from os import listdir
import ipywidgets as widgets
from datetime import datetime
from tkinter import Tk
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
import time
from PIL import Image
import json

from .temp import Temp
from .task import *

OUTPUT_DIR = "./output"
EMPTY_IMAGE = "data:image/gif;base64,R0lGODlhAQABAAAAACwAAAAAAQABAAA="


class View(object):
    def __init__(self, *args, height=600, cols=1, zoom=7, link=False):
        self.tid_list = list(args)  # tempset id list (integer)
        self.maps = []  # list of View.Map objects
        self.layers = []  # list of View.Layer objects
        self.cont = None  # View.Content object
        self.ctrl = None  # View.Control object
        self.height = height  # height of view
        self.cols = cols  # number of map columns in one view
        self.zoom = zoom  # initial zoom level
        self.center = None
        self.link = link

        # in case that input only contains one id
        if len(self.tid_list) == 1:
            if type(self.tid_list[0]) is int:
                self.single_view()
                return
            else:
                self.tid_list = list(self.tid_list[0])

        # in case that input contains more than one ids
        views = []
        for i in self.tid_list:
            views.append(View(i, height=self.height, zoom=self.zoom))
        if self.cols == 1:
            self.superposed_view(views)
        elif self.cols > 1:
            self.juxtaposed_view(views)
            if self.link:
                self.link_maps(views)

    def single_view(self):
        """
        Render view with a single tempset id as input
        :return:
        """
        profile = Temp(self.tid_list[0]).profile
        self.center = profile["task"]["options"]["Center"]

        # initialize maps, layers, content and control object
        self.maps.append(
            self.Map(
                center=self.center,
                height=self.height,
                zoom=self.zoom,
                tid=str(self.tid_list),
                parent=self
            )
        )
        self.layers.append(
            self.Layer(profile)
        )
        self.cont = self.Content(self.cols)
        self.ctrl = self.Control()

        # add layer to basemap
        self.maps[0].add_layer(self.layers[0])

        # add basemap to content
        self.cont.add_content(self.maps[0])

        # add control to view
        self.ctrl.add_control(self)

    def superposed_view(self, views):
        """
        Superpose multiple views in one view
        :param views:
        :return:
        """
        centers = [v.center for v in views]
        self.center = [
            sum([abs(c[0]) for c in centers]) / len(centers),
            sum([abs(c[1]) for c in centers]) / len(centers),
        ]

        # initialize maps, layers, content and control object
        self.maps.append(
            self.Map(
                center=self.center,
                height=self.height,
                zoom=self.zoom,
                tid=str(self.tid_list),
                parent=self
            )
        )
        for view in views:
            self.layers = self.layers + view.layers
        self.cont = self.Content(self.cols)
        self.ctrl = self.Control()

        # add layers to basemap
        for layer in self.layers:
            self.maps[0].add_layer(layer)

        # add basemap to content
        self.cont.add_content(self.maps[0])

        # add control to views
        self.ctrl.add_control(self)

    def juxtaposed_view(self, views):
        """
        Juxtapose multiple views in grid layout
        :param views:
        :return:
        """
        # initialize maps, layers, content and control object
        for view in views:
            self.maps = self.maps + view.maps
            self.layers = self.layers + view.layers

        for map in self.maps:
            map.set_parent(self)

        self.cont = self.Content(self.cols)
        self.ctrl = self.Control()

        # add maps to content
        for m in self.maps:
            self.cont.add_content(m)

        # add control to views
        self.ctrl.add_control(self)

    def set_zoom(self, zoom):
        """
        Set zoom level of all the maps in the view
        :param zoom:
        :return:
        """
        for map in self.maps:
            map.set_zoom(zoom)

    def set_center(self, center):
        """
        Set center of all the maps in the view
        :param center:
        :return:
        """
        for map in self.maps:
            map.set_center(center)

    def get_map_ids(self):
        """
        Get the list of map ids
        :return:
        """
        return [m.tid for m in self.maps]

    def mask_to(self, masks, target_map_tid):
        """
        Add mask layers to target map
        :param mask_list:
        :param target_map_tid:
        :return:
        """
        # determine the target map
        for map in self.maps:
            if map.tid == target_map_tid:
                break

        # add masks to the target map
        for mask in masks:
            map.add_mask(mask)


    @staticmethod
    def link_maps(views):
        """Link base maps on zoom level and center
        :return:
        """
        # unify zoom and center of all the views
        centers = [view.center for view in views]
        zooms = [view.zoom for view in views]
        unified_center = [
            sum([abs(c[0]) for c in centers]) / len(centers),
            sum([abs(c[1]) for c in centers]) / len(centers),
        ]
        unified_zoom = min(zooms)
        for view in views:
            view.set_center(unified_center)
            view.set_zoom(unified_zoom)

        # link change map center and zoom
        def change_center(cc):
            for view in views:
                view.set_center(cc["new"])

        def change_zoom(cz):
            for view in views:
                view.set_zoom(cz["new"])

        for view in views:
            for map in view.maps:
                map.w_map.observe(change_center, names="center")
                map.w_map.observe(change_zoom, names="zoom")

    def get(self):
        """
        Return the whole view including maps and corresponding controls
        :return:
        """
        return widgets.VBox([
            self.cont.w_container,
            self.ctrl.w_container,
        ])

    class Map(object):
        """
        class definition of basemap
        """
        def __init__(self, center, height, zoom, tid=None, parent=None):
            self.center = center
            self.layers = []  # list of layers embedded to this map
            self.masks = []  # list of masks embeeded to this map
            self.legend_infos = []
            self.legend_image_paths = []
            self.selections = {}  # dict of selections, where values are markers
            self.test = None
            self.tid = tid  # tid = None for superposed view
            self.parent = parent

            self.w_map = ill.Map(
                basemap=ill.basemaps.CartoDB.Positron,
                center=self.center,
                scroll_wheel_zoom=True,
                attribution_control=False,
                zoom=zoom,
                layout=widgets.Layout(height=str(height) + "px"),
            )

            # initialize widgets when displaying the map
            def display_map(dm):
                self.w_mask.options = self.parent.get_map_ids()
                self.w_mask.value = None

            self.w_map.on_displayed(display_map)

            """
            ### Top-left controls   
            """

            # fullscreen
            self.w_map.add_control(ill.FullScreenControl())

            # data selection
            self.w_select = ill.DrawControl(
                circle={}, circleMarker={}, circlemarker={},
                CircleMarker={}, polyline={}, marker={}, polygon={},
            )
            self.w_select.rectangle = {
                "shapeOptions": {
                    "fillColor": "None",
                    "color": "black",
                    "opacity": 1,
                    "weight": 3,
                }
            }
            self.w_select.edit = False

            def handle_select(selector, action, geo_json):
                json_str = json.dumps(geo_json)
                if action == "created":
                    marker_pos = geo_json["geometry"]["coordinates"][0][1]
                    marker_pos = [marker_pos[1], marker_pos[0]]
                    icon = ill.AwesomeIcon(
                        name="info",
                        marker_color="black",
                        icon_color="white"
                    )
                    popup = ill.Popup(
                        child=widgets.VBox(
                            layout=widgets.Layout(width="150px")
                        ),
                        close_on_escape_key=False,
                        auto_pan=True,
                        auto_close=False
                    )
                    marker = ill.Marker(
                        icon=icon,
                        popup=popup,
                        location=marker_pos,
                        draggable=False,
                    )

                    def click_marker(*args, **kwargs):
                        self.select(geo_json=geo_json)

                    marker.on_click(click_marker)

                    self.w_map.add_layer(marker)
                    self.selections[json_str] = marker
                elif action == "deleted":
                    marker = self.selections[json_str]
                    self.w_map.remove_layer(marker)
                    del self.selections[json_str]

            self.w_select.on_draw(handle_select)
            self.w_map.add_control(self.w_select)

            # export
            self.w_export = widgets.Button(
                icon="download",
                layout=widgets.Layout(
                    width="30px",
                    padding="5px"
                )
            )

            def click_export(b):
                self.export()

            self.w_export.on_click(click_export)
            self.w_map.add_control(
                ill.WidgetControl(widget=self.w_export, position="topleft", )
            )

            """ 
            ### Bottom-left controls 
            """

            # map label
            self.w_label = widgets.HTML(
                value="<b>Map of Tempset " + self.tid + "</b>",
                layout=widgets.Layout(margin="0px 0px 0px 10px")
            )

            # alpha
            self.w_alpha = widgets.FloatSlider(
                value=1, min=0, max=1, step=0.05,
                description="Alpha",
                orientation="horizontal",
                layout=widgets.Layout(width="200px", padding="10px"),
                style={'description_width': 'initial'}
            )

            def change_alpha(ca):
                for layer in self.layers:
                    layer.w_layer.opacity = ca["new"]

            self.w_alpha.observe(change_alpha, names="value")

            # filter
            self.w_filter = widgets.IntRangeSlider(
                value=[0, 1], min=0, max=1, step=1,
                description="Filter",
                orientation="horizontal",
                layout=widgets.Layout(width="200px", padding="10px"),
                style={'description_width': 'initial'}
            )

            def change_filter(cf):
                vmin, vmax = self.w_filter.value
                for layer in self.layers:
                    layer.update_filter(vmin, vmax)
                    layer.value_filter_current()

            self.w_filter.observe(change_filter, names="value")

            self.w_filter_apply = widgets.Button(
                description="Apply",
                layout=widgets.Layout(
                    width="50px",
                    height="25px",
                    padding="0px 0px 0px 0px",
                    margin="0px 5px 0px 0px"
                )
            )

            def click_filter_apply(cfa):
                print("[INFO] Applying filter over time......", end="")
                for layer in self.layers:
                    if layer.isFiltered() and layer.isFilterChanged():
                        layer.value_filter_full()
                print("Done!")

            self.w_filter_apply.on_click(click_filter_apply)

            # mask
            self.w_mask = widgets.Dropdown(
                description="Mask",
                style={'description_width': 'initial'},
                layout=widgets.Layout(
                    width="180px",
                    margin="0px 0px 8px 12px"
                )
            )

            self.w_mask_apply = widgets.Button(
                description="Apply",
                layout=widgets.Layout(
                    width="50px",
                    height="25px",
                    padding="0px 0px 0px 0px",
                    margin="0px 10px 0px 12px"
                )
            )

            def click_mask_apply(cma):
                # check if filtering is done
                if not self.layers[0].isFiltered() or self.w_mask.value is None:
                    print("[INFO] Please firstly perform filtering and "
                          "determine the target map.")
                    return

                # create mask layers and send to its parent view
                masks = []
                target_map_tid = self.w_mask.value
                for layer in self.layers:
                    masks.append(View.Mask(layer.profile, layer.current_image))
                self.parent.mask_to(masks=masks, target_map_tid=target_map_tid)

            self.w_mask_apply.on_click(click_mask_apply)

            # add all the controls to the map
            self.w_map.add_control(
                ill.WidgetControl(
                    widget=widgets.VBox([
                        self.w_label,
                        self.w_alpha,
                        widgets.HBox([self.w_filter, self.w_filter_apply]),
                        widgets.HBox([self.w_mask, self.w_mask_apply])
                    ]),
                    position="bottomleft",
                )
            )

        def set_zoom(self, zoom):
            """
            Set zoom level of basemap
            :param zoom:
            :return:
            """
            self.w_map.zoom = zoom

        def set_center(self, center):
            """
            Set center of basemap
            :param center:
            :return:
            """
            self.w_map.center = center

        def set_parent(self, parent):
            """
            Set parent view of the map
            :param parent:
            :return:
            """
            self.parent = parent

        def add_layer(self, layer):
            """
            Add View.Layer object ot map
            :param layer:
            :return:
            """
            # add layer to map
            self.layers.append(layer)
            self.w_map.add_layer(layer.w_layer)

            # add legend to map if exists
            if layer.legend_info not in self.legend_infos and \
                    layer.legend_info is not None:
                self.legend_infos.append(layer.legend_info)
                self.legend_image_paths.append(layer.legend_image_path)

                # update value range of filter
                vmin, vmax = layer.legend_info[1][1]
                vmin = round(vmin)
                vmax = round(vmax)
                self.w_filter.min = vmin
                self.w_filter.max = vmax
                self.w_filter.value = [vmin, vmax]

                # add legend widget to basemap
                self.w_map.add_control(
                    ill.WidgetControl(
                        widget=widgets.Image(
                            value=open(layer.legend_image_path, "rb").read(),
                            format="png",
                            layout=widgets.Layout(height="100px", width="40px")
                        ),
                        position="topright",
                    )
                )

        def add_mask(self, mask):
            """
            add View.Mask object to map
            :param mask:
            :return:
            """
            self.masks.append(mask)
            self.w_map.add_layer(mask.w_mask)

        def export(self, output_dir=OUTPUT_DIR):
            """
            Export map as .mp4 video
            :return:
            """
            map_height = self.w_map.layout.height
            map_height = int(map_height.replace("px", ""))

            # initialize web driver, only Chrome
            options = webdriver.ChromeOptions()
            options.add_argument('--headless')
            options.add_argument("--log-level=3")
            driver = webdriver.Chrome(
                executable_path=ChromeDriverManager().install(),
                chrome_options=options
            )
            print("[INFO] Exporting as .mp4 movie......", end="")

            # get the whole list of temp image
            temp_image_names = []
            for layer in self.layers:
                temp_image_names += layer.temp_image_names
            temp_image_names = list(set(temp_image_names))
            temp_image_names.sort()

            # create frames
            frame_html_path = os.path.join(output_dir, "frame.html")
            for temp_image_name in temp_image_names:
                temp_map = folium.Map(
                    location=self.w_map.center,
                    zoom_start=self.w_map.zoom,
                    tiles="cartodbpositron"
                )

                # add layers
                for layer in self.layers:
                    if not layer.isFiltered():
                        image_dir = layer.temp_image_dir
                    else:
                        image_dir = os.path.join(
                            layer.temp_image_dir,
                            "filter_results"
                        )
                    target_temp_image_path = os.path.join(
                        image_dir,
                        temp_image_name
                    )
                    if os.path.exists(target_temp_image_path):
                        image_uri = pathlib.Path(
                            os.path.abspath(target_temp_image_path)
                        ).as_uri()
                        image_bounds = layer.profile["task"]["options"][
                            "Bounds"]
                        image_opacity = layer.w_layer.opacity
                        folium.raster_layers.ImageOverlay(
                            image=image_uri,
                            bounds=image_bounds,
                            opacity=image_opacity
                        ).add_to(temp_map)

                # add timestamp
                time_image_path = os.path.join(output_dir, "time.png")
                time_image_uri = pathlib.Path(
                    os.path.abspath(time_image_path)
                ).as_uri()
                plt.figure(figsize=(1.7, 0.2))
                plt.text(0, 0, temp_image_name.split(".")[0],
                         fontsize=12)
                plt.axis("off")
                plt.savefig(time_image_path, bbox_inches="tight",
                            pad_inches=0.02)
                plt.close()
                timestamp = FloatImage(time_image_uri, bottom=0,
                                       left=0)
                timestamp.add_to(temp_map)

                # add legend
                for legend_image_path in self.legend_image_paths:
                    legend_image_uri = pathlib.Path(
                        os.path.abspath(legend_image_path)
                    ).as_uri()
                    FloatImage(
                        legend_image_uri,
                        bottom=5,
                        left=0
                    ).add_to(temp_map)

                # get frame image
                temp_map.save(frame_html_path)
                driver.get(
                    pathlib.Path(
                        os.path.abspath(frame_html_path)
                    ).as_uri()
                )
                driver.set_window_size(
                    1000,
                    map_height,
                )
                time.sleep(1)
                driver.save_screenshot(
                    os.path.join(output_dir, temp_image_name)
                )
            driver.quit()

            # form frames to video
            frame_image_path_list = [img for img in os.listdir(
                output_dir) if img.endswith(".png")]
            frame_image_path_list.sort()
            video_name = "video_" + datetime.now().strftime(
                "%Y%m%d_%H%M%S") + ".mp4"
            frame = cv2.imread(os.path.join(
                output_dir, frame_image_path_list[0]))
            height, width, layers = frame.shape
            fourcc = cv2.VideoWriter_fourcc('m', 'p', '4', 'v')
            video = cv2.VideoWriter(
                os.path.join(output_dir, video_name),
                fourcc, 10, (width, height))
            for image in frame_image_path_list:
                video.write(
                    cv2.imread(os.path.join(output_dir, image)))
            cv2.destroyAllWindows()
            video.release()

            # remove temp files
            os.remove(os.path.join(output_dir, "frame.html"))
            os.remove(os.path.join(output_dir, "time.png"))
            for temp_image_name in temp_image_names:
                os.remove(os.path.join(output_dir, temp_image_name))

            print("Done!")
            print("[INFO] Video path:", os.path.join(output_dir, video_name))

        def select(self, geo_json):
            """
            Callback function that handle the data selection on basemap
            :param geo_json:
            :return:
            """
            json_str = json.dumps(geo_json)
            marker = self.selections[json_str]
            if len(marker.popup.child.children) > 0:
                return
            marker.popup.child.children += (
                widgets.HTML(value="<b>Statistical Overview</b>"),
            )
            data_list = {}

            # Determine the bbox of the selected region
            vertices = geo_json["geometry"]["coordinates"][0]
            lats = [v[1] for v in vertices]
            lons = [v[0] for v in vertices]
            selected_bbox = [
                [min(lats), min(lons)],
                [max(lats), max(lons)]
            ]

            # get and summarize data for all the layers
            for layer in self.layers:
                data_selected = layer.get_selected_data(selected_bbox)
                if data_selected is None: continue
                name = layer.profile["task"]["name"]
                mean = "{:.3f}".format(np.nanmean(data_selected))
                median = "{:.3f}".format(np.nanmedian(data_selected))
                sd = "{:.3f}".format(np.nanstd(data_selected))
                layer_info = "<p style='line-height:15px'>"
                layer_info += "Layer: " + name + "<br>"
                layer_info += "Mean: " + mean + "<br>"
                layer_info += "Median: " + median + "<br>"
                layer_info += "SD: " + sd + "<br></p>"
                marker.popup.child.children += (
                    widgets.HTML(layer_info),
                )
                data_list[name] = data_selected

                # download link
                file_name = name + ".csv"
                df = pd.DataFrame(data=data_selected)
                csv = df.to_csv(na_rep="NULL", index=False, header=False)
                b64 = b64encode(csv.encode())
                payload = b64.decode()

                html = '<a download="{filename}" href="data:text/csv;base64,' \
                       '{payload}" target="_blank">Download Data</a>'
                html = html.format(payload=payload, filename=file_name)
                marker.popup.child.children += (
                    widgets.HTML(html),
                )

    class Mask(object):
        """
        Class definition of mask layer
        """

        def __init__(self, profile, current_image):
            self.profile = profile
            self.mask_image_dir = os.path.join(
                profile["temp_set_path"], "images/masks"
            )

            # create list of mask image names
            self.mask_image_names = []
            for f in os.listdir(self.mask_image_dir):
                fp = os.path.join(self.mask_image_dir, f)
                if os.path.isfile(fp) and not f.startswith(".") and f != \
                        "legend.png":
                    self.mask_image_names.append(f)
            self.mask_image_names.sort()
            self.current_image = current_image

            # create mask layer widget
            image_bounds = self.profile["task"]["options"]["Bounds"]
            first_mask_image_path = os.path.join(
                self.mask_image_dir,
                self.current_image
            )
            self.w_mask = ill.ImageOverlay(
                url=read_image(first_mask_image_path),
                bounds=image_bounds,
                opacity=1,
            )

    class Layer(object):
        """
        Class definition of data layer
        """

        def __init__(self, profile):
            self.profile = profile
            self.temp_image_dir = os.path.join(
                profile["temp_set_path"], "images"
            )
            self.temp_data_dir = os.path.join(
                profile["temp_set_path"], "data"
            )

            # get list of temp image names
            self.temp_image_names = []
            for f in os.listdir(self.temp_image_dir):
                fp = os.path.join(self.temp_image_dir, f)
                if os.path.isfile(fp) and not f.startswith(".") and f != \
                        "legend.png":
                    self.temp_image_names.append(f)
            self.temp_image_names.sort()
            self.current_image = self.temp_image_names[0]

            # get data layer widget
            source = self.profile["task"]["source"]
            data_sources = Task.data_sources
            if source == data_sources[0] or source == data_sources[3]:
                image_bounds = self.profile["task"]["options"]["Bounds"]
                first_temp_image_path = os.path.join(
                    self.temp_image_dir,
                    self.temp_image_names[0]
                )
                self.w_layer = ill.ImageOverlay(
                    url=read_image(first_temp_image_path),
                    bounds=image_bounds,
                    opacity=1,
                )
            else:
                self.w_layer = None

            # get legend information
            if "Colormap" in self.profile["task"]["options"]:
                self.legend_info = (
                    "Colormap",
                    self.profile["task"]["options"]["Colormap"]
                )
            else:
                self.legend_info = None

            # get legend image path
            self.legend_image_path = os.path.join(
                self.temp_image_dir, "legend.png"
            )
            legend_info = self.legend_info
            if legend_info[0] == "Colormap":
                if source == data_sources[0]:
                    ticks = None
                else:
                    ticks = [0.1, 0, 1, 10, 100, 1000, 10000]
                cmap = legend_info[1][0]
                value_min, value_max = legend_info[1][1]
                norm_type = legend_info[1][2]

                # create temp image of color bar
                if cmap == "rbc":
                    cmap = RBC_COLORMAP
                get_colormap_image(
                    cmap=cmap, norm_type=norm_type,
                    vmin=value_min, vmax=value_max,
                    output_path=self.legend_image_path,
                    ticks=ticks
                )

            # determine the filter state
            self.vmin = value_min
            self.vmax = value_max

        def update_filter(self, vmin, vmax):
            """
            Update filter state
            :param vmin:
            :param vmax:
            :return:
            """
            self.vmin = vmin
            self.vmax = vmax

            # clean filter result due to change of filter
            filter_result_dir = os.path.join(
                self.temp_image_dir, "filter_results"
            )
            shutil.rmtree(filter_result_dir)
            os.mkdir(filter_result_dir)

            # clean masks due to change of filter
            mask_dir = os.path.join(
                self.temp_image_dir, "masks"
            )
            shutil.rmtree(mask_dir)
            os.mkdir(mask_dir)

        def isFilterChanged(self):
            """
            determine if filter is changed
            :return:
            """
            filter_result_dir = os.path.join(
                self.temp_image_dir, "filter_results"
            )
            if not os.listdir(filter_result_dir):
                return True
            else:
                return False

        def isFiltered(self):
            """
            determine if layer is filtered
            :return:
            """
            filter_left = self.vmin > self.legend_info[1][1][0]
            filter_right = self.vmax < self.legend_info[1][1][1]
            return filter_left or filter_right

        def value_filter_current(self):
            """
            Mask current layer at the current timestamp
            :return:
            """
            # if value filter is not activated, just show the current image
            if not self.isFiltered():
                current_image_path = os.path.join(
                    self.temp_image_dir, self.current_image
                )
                self.w_layer.url = read_image(current_image_path)
                return

            # current image to src data
            image_path = os.path.join(
                self.temp_image_dir,
                self.current_image
            )
            src_data = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)

            # get value array to create mask data
            mask_path = os.path.join(
                self.temp_data_dir,
                self.current_image.split(".")[0] + ".h5"
            )
            with h5py.File(mask_path, "r") as f:
                mask_data = f["data"][...]

            # compute mask data
            if self.vmin > self.legend_info[1][1][0]:
                temp_left = (mask_data >= self.vmin)
            else:
                temp_left = np.ones(mask_data.shape, dtype=bool)
            if self.vmax < self.legend_info[1][1][1]:
                temp_right = (mask_data <= self.vmax)
            else:
                temp_right = np.ones(mask_data.shape, dtype=bool)
            mask_data = temp_left & temp_right
            mask_data = mask_data.astype(np.uint8)
            mask_data = mask_data * 255
            mask_data = cv2.merge((mask_data, mask_data,
                                   mask_data, mask_data))

            # generate masked array and output as image
            dst_data = cv2.bitwise_and(src_data, mask_data)
            r, buffer = cv2.imencode(".png", dst_data)
            self.w_layer.url = "data:image/png;base64," + b64encode(
                buffer).decode("ascii")

        def value_filter_full(self):
            """
            Mask current layer over time
            :return:
            """
            filter_result_dir = os.path.join(
                self.temp_image_dir, "filter_results"
            )
            mask_dir = os.path.join(
                self.temp_image_dir, "masks"
            )
            filter_left = self.vmin > self.legend_info[1][1][0]
            filter_right = self.vmax < self.legend_info[1][1][1]

            for image_name in self.temp_image_names:
                image_path = os.path.join(
                    self.temp_image_dir,
                    image_name
                )
                mask_path = os.path.join(
                    self.temp_data_dir,
                    image_name.split(".")[0] + ".h5"
                )
                src_data = cv2.imread(image_path, cv2.IMREAD_UNCHANGED)
                with h5py.File(mask_path, "r") as f:
                    mask_data = f["data"][...]

                # generate filter result data array
                if filter_left:
                    temp_left = (mask_data >= self.vmin)
                else:
                    temp_left = np.ones(mask_data.shape, dtype=bool)
                if filter_right:
                    temp_right = (mask_data <= self.vmax)
                else:
                    temp_right = np.ones(mask_data.shape, dtype=bool)
                mask_data = temp_left & temp_right
                mask_data = mask_data.astype(np.uint8)
                mask_data = mask_data * 255
                mask_data = cv2.merge((mask_data, mask_data,
                                       mask_data, mask_data))
                dst_data = cv2.bitwise_and(src_data, mask_data)

                # generate filter result images and mask images
                cv2.imwrite(
                    os.path.join(filter_result_dir, image_name), dst_data
                )
                mask_data[..., 3] = 255 - mask_data[..., 3]
                cv2.imwrite(
                    os.path.join(mask_dir, image_name), mask_data
                )

        def get_selected_data(self, select_region):
            """
            Get selected data
            :return:
            """
            # projection transfer of the bboxes
            inProj = Proj(init="epsg:4326")
            outProj = Proj(init="epsg:3857")

            src_bbox = self.profile["task"]["options"]["Bounds"]
            lon_min_src, lat_min_src = transform(
                inProj, outProj, src_bbox[0][1], src_bbox[0][0])
            lon_max_src, lat_max_src = transform(
                inProj, outProj, src_bbox[1][1], src_bbox[1][0])

            dst_bbox = copy.deepcopy(select_region)
            lon_min_dst, lat_min_dst = transform(
                inProj, outProj, dst_bbox[0][1], dst_bbox[0][0])
            lon_max_dst, lat_max_dst = transform(
                inProj, outProj, dst_bbox[1][1], dst_bbox[1][0])

            # compute bbox of intersection
            lat_min_int = max(lat_min_src, lat_min_dst)
            lon_min_int = max(lon_min_src, lon_min_dst)
            lat_max_int = min(lat_max_src, lat_max_dst)
            lon_max_int = min(lon_max_src, lon_max_dst)

            if lat_min_int >= lat_max_int or lon_min_int >= lon_max_int:
                return None  # if no intersection, return None

            # get current data file
            current_data = self.current_image.split(".")[0] + ".h5"
            current_data_path = os.path.join(
                self.temp_data_dir, current_data
            )
            with h5py.File(current_data_path, "r") as f:
                data = f["data"][:]

            # get selected data
            num_rows, num_cols = data.shape
            row_min = num_rows * (lat_max_src - lat_max_int) / (
                    lat_max_src - lat_min_src)
            row_max = num_rows * (lat_max_src - lat_min_int) / (
                    lat_max_src - lat_min_src)
            col_min = num_cols * (lon_min_int - lon_min_src) / (
                    lon_max_src - lon_min_src)
            col_max = num_cols * (lon_max_int - lon_min_src) / (
                    lon_max_src - lon_min_src)

            row_min = round(row_min)
            row_max = round(row_max)
            col_min = round(col_min)
            col_max = round(col_max)

            return data[row_min:row_max, col_min:col_max]

    class Content(object):
        """
        class definition of content
        """

        def __init__(self, col):
            width = round(100.0 / col, 2)
            template = ""
            for i in range(col):
                template += str(width) + "% "
            self.w_container = widgets.GridBox(
                children=[],
                layout=widgets.Layout(
                    grid_template_columns=template
                )
            )

        def add_content(self, map):
            """
            Add map to content
            :param map:
            :return:
            """
            self.w_container.children += (map.w_map,)

    class Control(object):
        """class definition of control

        Area that contains all the widgets for controlling the view.
        """

        def __init__(self):
            self.w_container = widgets.VBox(
                children=[]
            )
            self.player = None
            self.config = None

        def add_control(self, view):
            """
            Add control to the target view
            :param view:
            :return:
            """
            self.player = self.Player(view)

            self.w_container.children += (
                self.player.get(),
            )

        class Player(object):
            """
            class definition of animation player widget
            """

            def __init__(self, view):
                # unifiy timeline
                self.timeline = []
                for layer in view.layers:
                    timeline = [t.split(".")[0] for t in layer.temp_image_names]
                    timeline = [parser.parse(t) for t in timeline]
                    self.timeline += timeline
                self.timeline = list(set(self.timeline))
                self.timeline.sort()

                # init widgets
                self.w_player = widgets.Play(
                    value=0, min=0, max=len(self.timeline) - 1, interval=150
                )
                self.w_slider = widgets.SelectionSlider(
                    value=timeline[0], options=self.timeline
                )
                self.w_speed = widgets.Dropdown(
                    options=[("1", 450), ("2", 350), ("3", 250), ("4", 150),
                             ("5", 50)],
                    value=450,
                    description="Speed:",
                    layout=widgets.Layout(width="100px"),
                    style={"description_width": "45px"}
                )

                def update_layers(targe_image_name):
                    """
                    update layers based on widget changes
                    :param targe_image_name:
                    :return:
                    """
                    # update data layers
                    for layer in view.layers:
                        # if timestamp in layer's timeline
                        if targe_image_name in layer.temp_image_names:
                            layer.current_image = targe_image_name
                            if layer.isFiltered():
                                image_dir = os.path.join(
                                    layer.temp_image_dir, "filter_results"
                                )
                            else:
                                image_dir = layer.temp_image_dir
                            targe_image_path = os.path.join(
                                image_dir, layer.current_image
                            )
                            layer.w_layer.url = read_image(targe_image_path)
                        else:
                            layer.w_layer.url = EMPTY_IMAGE

                    # update mask layers
                    for map in view.maps:
                        for mask in map.masks:
                            if targe_image_name in mask.mask_image_names:
                                mask.current_image = targe_image_name
                                targe_image_path = os.path.join(
                                    mask.mask_image_dir, mask.current_image
                                )
                                mask.w_mask.url = read_image(targe_image_path)
                            else:
                                mask.w_mask.url = EMPTY_IMAGE

                def change_slider(cs):
                    i = self.timeline.index(cs["new"])
                    self.w_player.value = i
                    targe_image_name = cs["new"].strftime(
                        "%Y%m%d %H%M") + ".png"
                    update_layers(targe_image_name)

                self.w_slider.observe(change_slider, names="value")

                def change_player(cp):
                    t = self.timeline[cp["new"]]
                    self.w_slider.value = t
                    targe_image_name = t.strftime("%Y%m%d %H%M") + ".png"
                    update_layers(targe_image_name)

                self.w_player.observe(change_player, names="value")

                widgets.link(
                    (self.w_speed, "value"),
                    (self.w_player, "interval")
                )

            def get(self):
                """
                Get the player widget box
                :return:
                """
                return widgets.HBox(
                    [self.w_player, self.w_speed, self.w_slider],
                    layout=widgets.Layout(
                        width="100%",
                        flex_flow="row wrap"
                    )
                )
