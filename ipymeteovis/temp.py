"""
Class definition of temp set

Author: @Ji_Qi
"""
import shutil
import qgrid
import pandas as pd

from .task import *


def list():
    """Get list of temporary set
    :return:
    """
    return List_GUI().show()


class List_GUI(object):
    """GUI regarding the get method of temporary set
    """

    def __init__(self):
        df = self.refresh_temp()
        w_table = qgrid.show_grid(df, show_toolbar=False)
        w_refresh = widgets.Button(
            description="Refresh"
        )
        w_remove = widgets.Button(
            description="Remove"
        )

        def click_refresh(b):
            df = self.refresh_temp()
            w_table.df = df
        w_refresh.on_click(click_refresh)

        def click_remove(b):
            selection = w_table.get_selected_df()
            id_list = [i for i, t in selection.iterrows()]
            id_list.sort(reverse=True)
            for id in id_list:
                Temp(id).remove()
            click_refresh(None)
        w_remove.on_click(click_remove)

        self.container = widgets.VBox([
            widgets.HTML(value="<b style='font-size: medium'>List of "
                               "Temporary Sets</b>"),
            w_table,
            widgets.HBox([w_refresh, w_remove])
        ])

    def refresh_temp(self):
        """Update the temp list based on the existing temp sets.
        :return:
        """
        temp_list = Temp.get_temp_list()
        data = [Temp(i).profile["task"] for i in range(len(temp_list))]
        df = pd.DataFrame(data)
        return df

    def show(self):
        return self.container


class Temp(object):
    """Class definition of a temporary set.

    This is for instantiating temporary sets for creating views.
    """
    def __init__(self, id):
        self.id = id
        self.temp_path = self.get_temp_list()[id]

        with open(self.temp_path + "/profile.txt", "r") as f:
            self.profile = eval(f.read())

    def remove(self):
        """Remove this temporary set.
        :return:
        """
        shutil.rmtree(self.temp_path)

    @staticmethod
    def get_temp_list():
        """Get the path list of existing temp sets
        :return:
        """
        t_list = os.listdir(TEMP_SETS_DIR)
        t_list = [TEMP_SETS_DIR + "/" + d for d in t_list if
                  not d.startswith('.')]
        t_list.sort()
        return t_list

