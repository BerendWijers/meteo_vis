#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 13:31:00 2020

@author: Berend-Christiaan Wijers
"""

# %% Libraries

import multiprocessing as mp
import time
import numpy as np


# %% Functions
def task_numpy(a):
    """
    Task implemented using numpy
    """
    return np.power(a, a)


def task(a):
    """
    Task: Exponentiation
    """
    return a ** a


# %% Env variables

# %% Main
if __name__ == '__main__':
    num = 100000
    with mp.Pool(processes=mp.cpu_count()) as pool:
        start_time = time.time()
        result = pool.map_async(task, range(num))
        print("map_async: %f seconds" % (time.time() - start_time))

        start_time = time.time()
        result = [pool.apply_async(task, i) for i in range(num)]
        print("apply_async: %f seconds" % (time.time() - start_time))

    with mp.Pool(processes=mp.cpu_count()) as pool:
        start_time = time.time()
        result = [pool.apply_async(task, i) for i in range(num)]
        print("apply_async_new: %f seconds" % (time.time() - start_time))

    with mp.Pool(processes=mp.cpu_count()) as pool:
        start_time = time.time()
        result = pool.map_async(task_numpy, range(num))
        print("map_async_numpy: %f seconds" % (time.time() - start_time))

        start_time = time.time()
        result = [pool.apply_async(task_numpy, i) for i in range(num)]
        print("apply_async_numpy: %f seconds" % (time.time() - start_time))

    with mp.Pool(processes=mp.cpu_count()) as pool:
        start_time = time.time()
        result = [pool.apply_async(task_numpy, i) for i in range(num)]
        print("apply_async_new_numpy: %f seconds" % (time.time() - start_time))
