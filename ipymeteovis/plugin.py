"""
Classes developed for special purposes that is not directly related to the
core functionality of ipymeteovis

Author: @Ji_Qi
"""
import ipywidgets as widgets
import minio
import pandas as pd
import qgrid
import tempfile

from .toolkit import *

DOWNLOAD_DIR = "./download"


class Minio:
    """
    Remote access of Minio S3 server
    """

    def __init__(self):
        self.endpoint = ""
        self.access_key = ""
        self.secret_key = ""
        self.minio_client = None
        self.download_path = DOWNLOAD_DIR
        self.is_temp = False  # if files are downloaded to a temp directory

    def login(self):
        """Login interface

        Users need to input the endpoint of the minio s3 server, and his/her
        access and secret key.
        :return:
        """
        w_endpoint = widgets.Text(
            value="fnwi-s0.science.uva.nl:9001",
            # value="play.min.io",  # sample
            description="End Point:",
        )
        w_access_key = widgets.Text(
            # value="Q3AM3UQ867SPQQA43P2F",  # sample
            description="Access Key:"
        )
        w_secret_key = widgets.Password(
            # value="zuf+tfteSlswRu7BJ86wekitnifILbZam1KYY3TG",  # sample
            description="Secret Key:"
        )
        w_login = widgets.Button(
            description="Login",
            icon="check"
        )

        # handle click event of login button
        def click_login(b):
            self.endpoint = w_endpoint.value
            self.access_key = w_access_key.value
            self.secret_key = w_secret_key.value

            self.minio_client = minio.Minio(
                endpoint=self.endpoint,
                access_key=self.access_key,
                secret_key=self.secret_key,
                region="nl",
                secure=True,
            )
            print("[info] Successfully login!")

        w_login.on_click(click_login)

        # display
        container = widgets.VBox([
            widgets.HTML("<b style='font-size: medium>Minio S3: Remote Access</b>"),
            w_endpoint, w_access_key, w_secret_key, w_login
        ])
        return container

    def list(self):
        """Show a file list of a bucket

        User firstly select a bucket, and see a data frame showing all the
        files in that bucket. Users can sort and filter files by column(s).
        After selecting at least one file, user can download their selection
        by clicking download button.
        :return:
        """
        buckets = [b.name for b in self.minio_client.list_buckets()]
        w_bucket = widgets.Dropdown(
            options=buckets,
            value=None,
            description="Bucket:"
        )
        w_DOWNLOAD_DIR = widgets.Text(
            value=DOWNLOAD_DIR,
            description="Target: "
        )

        w_temp = widgets.Checkbox(
            value=False,
            description="Download to temp directory"
        )

        # listen to selecting bucket
        def change_bucket(change):
            # qgrid table
            bucket = change["new"]
            objects = self.minio_client.list_objects(
                bucket_name=bucket,
                prefix="/",
                recursive=True
            )
            df = pd.DataFrame([vars(f) for f in objects])
            w_table = qgrid.show_grid(df, show_toolbar=False)

            # download button
            w_download = widgets.Button(
                description="Download"
            )

            def click_download(b):
                selection = w_table.get_selected_df()
                range = selection.shape[0]
                pos = 0
                progress_bar(pos, range, w_output)

                for i, f in selection.iterrows():
                    pos += 1
                    bucket_name = f["bucket_name"]
                    object_name = f["object_name"]
                    file_name = object_name.split("/")[-1]
                    file_path = self.download_path + "/" + file_name
                    self.minio_client.fget_object(
                        bucket_name=bucket_name,
                        object_name=object_name,
                        file_path=file_path
                    )

                    progress_bar(pos, range, w_output)

            w_download.on_click(click_download)

            # output
            w_output = widgets.Output(layout=widgets.Layout(height="30px"))

            # update container
            container.children = tuple(list(container.children)[:3])
            container.children += (w_table, w_download, w_output,)

        w_bucket.observe(change_bucket, names="value")

        # listen to clicking temp checkbox
        def change_temp(change):
            if change["new"]:
                self.is_temp = True
                w_DOWNLOAD_DIR.disabled = True
                self.download_path = tempfile.mkdtemp()
            else:
                self.is_temp = False
                w_DOWNLOAD_DIR.disabled = False
                self.download_path = w_DOWNLOAD_DIR.value

        w_temp.observe(change_temp, names="value")

        # listen to changing download path
        def change_DOWNLOAD_DIR(change):
            self.download_path = change["new"]

        w_DOWNLOAD_DIR.observe(change_DOWNLOAD_DIR, names="value")

        container = widgets.VBox([
            widgets.HTML("<b style='font-size: medium>Bucket Overview</b>"),
            w_bucket,
            widgets.HBox([w_DOWNLOAD_DIR]),
        ])
        return container



