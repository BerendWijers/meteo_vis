"""
Toolkit functions

Author: @Ji_Qi
"""
import matplotlib.colors as colors
import matplotlib.pyplot as plt
import matplotlib as mpl
import linecache
import os
import tracemalloc
from base64 import b64encode
import math


def read_image(img_path):
    """
    read image file as base64 string
    :param img_path:
    :return:
    """
    with open(img_path, "rb") as img_file:
        result = "data:image/png;base64," + b64encode(
            img_file.read()).decode("ascii")
    return result


def distance(origin, destination):
    lat1, lon1 = origin
    lat2, lon2 = destination
    radius = 6371  # km

    dlat = math.radians(lat2 - lat1)
    dlon = math.radians(lon2 - lon1)
    a = math.sin(dlat / 2) * math.sin(dlat / 2) + math.cos(
        math.radians(lat1)) \
        * math.cos(math.radians(lat2)) * math.sin(dlon / 2) * math.sin(
        dlon / 2)
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))
    d = radius * c

    return d


def progress_bar(pos, range, output):
    """
    Print progress bar within ipywidget output

    :param output: ipywidget output
    :param pos: current position
    :param range: range of progress bar
    :return:
    """
    output.clear_output()
    with output:
        num = int(pos * 50 / range)
        progress = "Progress: "
        progress += "#" * num
        progress += "." * (50 - num)
        progress += " [" + str(num * 2) + "%]"
        print(progress)


def get_colormap_image(cmap, norm_type, vmin, vmax, output_path, ticks=None):
    """
    Generate image of given colormap

    :param cmap: colormap object or name as string if it's existing colormap
    :param norm_type: linear or log scale of normalization
    :param vmin: left extreme of value range
    :param vmax: right extreme of value range
    :param output_path: output file path
    :return:
    """
    # create norm
    if norm_type == "linear":
        norm = colors.Normalize(vmin=vmin, vmax=vmax)
    else:
        norm = colors.LogNorm(vmin=vmin, vmax=vmax)

    # create colormap image
    fig, ax = plt.subplots(figsize=(0.2, 2))
    fig.colorbar(
        mpl.cm.ScalarMappable(cmap=cmap, norm=norm),
        cax=ax, orientation="vertical", ticks=ticks
    )
    plt.savefig(output_path, bbox_inches="tight", pad_inches=0.02)
    plt.close()


def get_colormap_clutter():
    """
    Get the specific colormap of clutter flags

    :return:
    """
    color_list = []
    for r in range(8):
        for g in range(8):
            for b in range(8):
                color_list.append((r * 0.125, g * 0.125, b * 0.125, 1.0))
    cmap = colors.LinearSegmentedColormap.from_list(
        name="clutter",
        colors=color_list
    )
    return cmap


def get_colormap(color_list):
    """
    Get the colormap object

    :return:
    """
    cmap = colors.LinearSegmentedColormap.from_list(
        name="customized",
        colors=color_list,
        N=256
    )
    return cmap


def color_to_value(color):
    """
    Transfer the input color to the corresponding 3-3-3 value, which is in
    range of [0, 511].

    R, G, and B channels of the input color represents the first, second and
    final 3 digits of the result value, thus the output value is call 3-3-3
    value.

    :param color: color in either int or float
    :return: the result 3-3-3 value
    """
    # transfer int color to float color
    if type(color[0]) is int:
        color = [c / 255 for c in color]

    # get the original value of each channel
    color = [c * 8 for c in color]
    r = bin(int(color[0])).replace("0b", "")
    g = bin(int(color[1])).replace("0b", "")
    b = bin(int(color[2])).replace("0b", "")

    # fill the missing digits because of small value
    if len(r) < 3:
        gap = 3 - len(r)
        for i in range(gap):
            r = "0" + r
    if len(g) < 3:
        gap = 3 - len(g)
        for i in range(gap):
            g = "0" + g
    if len(b) < 3:
        gap = 3 - len(b)
        for i in range(gap):
            b = "0" + b

    rgb = r + g + b
    return int(rgb, 2)


def display_memory_usage(snapshot, key_type='lineno', limit=3):
    """
    Memory usage counter that firstly published here:
    https://stackoverflow.com/questions/552744/how-do-i-profile-memory-usage-in-python

    :param snapshot:
    :param key_type:
    :param limit:
    :return:
    """
    snapshot = snapshot.filter_traces((
        tracemalloc.Filter(False, "<frozen importlib._bootstrap>"),
        tracemalloc.Filter(False, "<unknown>"),
    ))
    top_stats = snapshot.statistics(key_type)

    print("Top %s lines" % limit)
    for index, stat in enumerate(top_stats[:limit], 1):
        frame = stat.traceback[0]
        # replace "/path/to/module/file.py" with "module/file.py"
        filename = os.sep.join(frame.filename.split(os.sep)[-2:])
        print("#%s: %s:%s: %.1f KiB"
              % (index, filename, frame.lineno, stat.size / 1024))
        line = linecache.getline(frame.filename, frame.lineno).strip()
        if line:
            print('    %s' % line)

    other = top_stats[limit:]
    if other:
        size = sum(stat.size for stat in other)
        print("%s other: %.1f KiB" % (len(other), size / 1024))
    total = sum(stat.size for stat in top_stats)
    print("Total allocated size: %.1f KiB" % (total / 1024))
