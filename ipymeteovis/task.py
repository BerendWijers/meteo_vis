"""
Class definitions of various tasks and the central control

The control class is for communicating with the front-end GUI, and to
organize the task into parallel.
Different tasks are defined by different classes. The "task" class is a unique
entrance that leads to different task instance based on the input task.
Each task class defines a tests and a create temp method as the two major
steps of the background computation. Content will be different.

Author: @Ji_Qi
"""
import os
import time
from functools import partial
import multiprocessing as mp
import copy
import h5py
import wradlib as wrl
import numpy as np
from dateutil import parser
from copy import deepcopy
from pyproj import Proj, transform
from matplotlib.patches import Ellipse
import matplotlib.image as mpimg
import gc
import ipywidgets as widgets
from IPython.display import display
import shutil
import math
import gzip

from .toolkit import *
from .plugin import *

TEMP_SETS_DIR = "./temp_sets"
RBC_COLORMAP = get_colormap(["lightblue", "darkblue", "lightgreen",
                             "yellow", "red", "magenta"])


def process(data_source, num_cores=mp.cpu_count()):
    """
    Receive data input and perform a specific task.
    Data source can be local files or remote Minio server.

    :param data_source: can be either folder path for local files,
    or plugin.Minio object if download data from Minio S3 server
    :param num_cores: number of cores being used
    :return: GUI widget
    """
    if num_cores > mp.cpu_count():
        print("[WARNING] You can use maximum %d cores!" % mp.cpu_count())
        return
    return Make_GUI(data_source, num_cores).container


class Make_GUI(object):
    """
    GUI of performing specifc task
    """

    def __init__(self, data_source, num_cores):
        # config
        config = {
            "name": "DEFAULT_NAME",
            "desc": "DEFAULT_DESC",
            "source": None,
            "options": None,
        }

        # check if data source is a folder path or a plugin.Minio object
        if type(data_source) is str:
            data_dir = data_source
        elif type(data_source) is Minio:
            data_dir = data_source.download_path

        # define process control
        process_control = Control(data_dir, num_cores)

        # initialize widgets of the GUI
        w_title = widgets.HTML(
            value="<b style='font-size: medium'>"
                  "Process Data to Temporary Set</b>"
                  "<p style='color: #D84141'>Please confirm before operating "
                  "that all the files in the above directory follows the "
                  "same scheme.</p>"
        )
        w_name = widgets.Text(
            placeholder="Name of the temporary set...",
            description="Name"
        )
        w_desc = widgets.Textarea(
            placeholder="Description of the temporary set...",
            description="Description"
        )
        w_source = widgets.Dropdown(
            options=Task.data_sources,
            description="Data Source",
            value=None
        )
        w_options = widgets.VBox()
        w_submit = widgets.Button(
            description="Submit Task",
            icon="check",
        )
        w_output = widgets.Output()

        # change task
        def change_task(ct):
            # get options
            options = process_control.get_task_options(ct["new"])
            if options is None:
                return
            else:
                config["source"] = ct["new"]
                config["options"] = {}

            # change option value
            def change_option_value(cov):
                for option in options:
                    if cov["owner"].description == option["description"]:
                        config["options"][option["key"]] = cov["new"]
                        break

            # add widgets based on options
            w_options.children = ()
            for option in options:
                if option["type"] == "dropdown":
                    w = widgets.Dropdown(
                        options=option["options"],
                        description=option["description"]
                    )
                w_options.children += (w,)
                config["options"][option["key"]] = w.value
                w.observe(change_option_value, names="value")

        w_source.observe(change_task, names="value")

        # Click event of submit
        def click_submit(b):
            w_output.clear_output()
            with w_output:
                process_control.submit_task(config)

        w_submit.on_click(click_submit)

        # change name and description
        def change_info(change):
            id = change["owner"].description
            if id == "Name":
                config["name"] = change["new"]
            elif id == "Description":
                config["desc"] = change["new"]

        w_name.observe(change_info, names="value")
        w_desc.observe(change_info, names="value")

        # add all the widgets to the container
        self.container = widgets.VBox([
            w_title, w_name, w_desc, w_source, w_options, w_submit, w_output
        ])


class Control(object):
    """
    Communication between the make GUI and the background program

    Logic of executing a task: first select a task from GUI, then the task
    name will be sent to the controller, who will get the options of that
    task and return it to the GUI for users to config. After submitting task
    from GUI, the task with config will be sent back again to the task
    objects for executing.
    """

    def __init__(self, data_dir, num_cores):
        self.data_dir = data_dir
        self.num_cores = num_cores
        self.file_path_list = []
        for r, d, fs in os.walk(self.data_dir):
            fs = [os.path.join(r, f) for f in fs if not f.startswith('.')]
            self.file_path_list += fs

    def get_task_options(self, source):
        """
        Get options applied to the input task

        Different options are required by RasterPolar and RasterCartesian
        :param source: task string, should be one of Task.data_sources
        :return:
        """
        return Task(
            file_path=self.file_path_list[0],  # sample file
            source=source
        ).get_options()

    def submit_task(self, config):
        """
        Initialize task objects and submit tasks

        :param config: config dict
        :return:
        """
        # create temp set directory
        temp_set_dir = TEMP_SETS_DIR + "/" + str(time.time_ns())
        temp_image_dir = temp_set_dir + "/images"
        temp_filter_result_dir = temp_image_dir + "/filter_results"
        temp_mask_dir = temp_image_dir + "/masks"
        temp_data_dir = temp_set_dir + "/data"
        os.mkdir(temp_set_dir)
        os.mkdir(temp_image_dir)
        os.mkdir(temp_filter_result_dir)
        os.mkdir(temp_mask_dir)
        os.mkdir(temp_data_dir)

        # Jobs in parallel
        print("[INFO] Processing......", end="")
        job = partial(
            Task,
            source=config["source"],
            config=config,
            temp_set_dir=temp_set_dir,
        )
        args = self.file_path_list
        self.parallel(job, args)

        # create profile
        sample_task = Task(self.file_path_list[0], config["source"])
        profile = {
            "temp_set_path": os.path.abspath(temp_set_dir),
            "task": sample_task.get_profile(config)
        }
        with open(temp_set_dir + "/profile.txt", 'w') as f:
            print(profile, file=f)
        print("Done!")

    def parallel(self, job, args):
        """
        Map job on args in parallel

        Parallel policy: split jobs into rounds, where each round have the
        same number of jobs as the number of cores.

        Originally we expect the maxtasksperchild arg of Pool conductor to do
        that job, but seems not working.
        :param job: job function
        :param args: iterator of args
        :return:
        """
        num_jobs = len(args)
        num_process = self.num_cores
        f_part, i_part = math.modf(num_jobs / num_process)
        i_part = int(i_part)
        start = 0

        # rounds running fully num_process jobs
        for i in range(i_part):
            round_index_list = list(range(start, start + num_process, 1))
            round_args = [args[id] for id in round_index_list]
            with mp.Pool(processes=num_process) as pool:
                pool.map(job, round_args)
            start += num_process

        # last round
        if start < num_jobs:
            round_index_list = list(range(start, num_jobs, 1))
            round_args = [args[id] for id in round_index_list]
            with mp.Pool(processes=len(round_index_list)) as pool:
                pool.map(job, round_args)


class Task(object):
    """Entrance to class definitions for different visual tasks
    """
    data_sources = [
        "OPERA weather radar data",
        "KNMI HDF5 data",
        "ERA5 NetCDF data",
        "RBC data",
    ]

    def __new__(cls, file_path, source, config=None, temp_set_dir=None):
        if source == cls.data_sources[0]:
            return Opera(file_path, config, temp_set_dir)
        elif source == cls.data_sources[3]:
            return RBC(file_path, config, temp_set_dir)


class RBC:
    def __init__(self, file_path, config, temp_set_dir):
        self.file_path = file_path
        self.data = None
        self.bounds = None
        self.center = None
        self.v_min = 0.1
        self.v_max = 10000
        self.dt = None

        # if config and temp_set_dir is set to be None,
        # this object is only for getting data profile
        if config is not None and temp_set_dir is not None:
            self.process(config=config)
            self.create_temp_image(temp_set_dir=temp_set_dir)
            self.create_temp_data_file(temp_set_dir=temp_set_dir)

    def process(self, config):
        """
        Process files to data value and grid

        :param config: config dict
        :return:
        """
        qty = config["options"]["qty"]
        with h5py.File(self.file_path, "r") as f:
            self.data = f["dataset1"][qty]["data"][...]

            lon_min = f["dataset1"]["how"].attrs["lon_min"]
            if isinstance(lon_min, np.ndarray): lon_min = lon_min[0]
            lon_min = float(lon_min)

            lon_max = f["dataset1"]["how"].attrs["lon_max"]
            if isinstance(lon_min, np.ndarray): lon_max = lon_max[0]
            lon_max = float(lon_max)

            lat_min = f["dataset1"]["how"].attrs["lat_min"]
            if isinstance(lat_min, np.ndarray): lat_min = lat_min[0]
            lat_min = float(lat_min)

            lat_max = f["dataset1"]["how"].attrs["lat_max"]
            if isinstance(lat_max, np.ndarray): lat_max = lat_max[0]
            lat_max = float(lat_max)

            radar_lat = f["dataset1"]["how"].attrs["radar_lat"]
            if isinstance(radar_lat, np.ndarray): radar_lat = radar_lat[0]
            radar_lat = float(radar_lat)

            radar_lon = f["dataset1"]["how"].attrs["radar_lon"]
            if isinstance(radar_lon, np.ndarray): radar_lon = radar_lon[0]
            radar_lon = float(radar_lon)

            tp = f["dataset1"]["how"].attrs["time"]
            if isinstance(tp, np.ndarray): tp = tp[0]
            tp = tp.decode("utf-8")

        # mask 0 value with nan
        self.data[self.data == 0] = np.nan

        # datatime stamp
        self.dt = parser.parse(tp)
        self.dt = self.dt.replace(second=0, microsecond=0)  # ignore second
        self.dt = self.dt.strftime("%Y%m%d %H%M")  # transfer back to str

        # get bbox and center
        self.bounds = [
            [lat_min, lon_min],
            [lat_max, lon_max]
        ]
        self.center = [radar_lat, radar_lon]

    def create_temp_data_file(self, temp_set_dir=None):
        """
        Temp data file keeps the cell values only. This is for filtering and
        masking in views.
        :param temp_set_dir:
        :return:
        """
        temp_data_dir = os.path.join(temp_set_dir, "data")

        # save as .h5 with compression_opts = 9 (output 1.4 MB per file)
        temp_data_path = os.path.join(temp_data_dir, self.dt + ".h5")
        with h5py.File(temp_data_path, "w") as f:
            f.create_dataset("data", data=self.data,
                             compression="gzip", compression_opts=9)

    def create_temp_image(self, temp_set_dir=None):
        """
        RBC temp images. Coordinate system is 2d Cartesian coordinate system,
        without any rotation.
        :return:
        """
        temp_image_dir = os.path.join(temp_set_dir, "images")
        temp_image_path = os.path.join(temp_image_dir, self.dt + ".png")

        # Set the resolution of the output figure to be the same as the
        # resolution of the data.
        fig_size = (self.data.shape[1], self.data.shape[0])
        fig = plt.figure(figsize=fig_size, dpi=1)
        fig.add_axes([0, 0, 1, 1])
        plt.axis("off")
        plt.imshow(self.data, 
                    # cmap=RBC_COLORMAP,
                    cmap="hot_r", 
                   norm=colors.LogNorm(vmin=self.v_min, vmax=self.v_max))
        fig.savefig(temp_image_path, transparent=True, bbox_inches="tight",
                    pad_inches=0, dpi=1)
        plt.close()

    def get_options(self):
        """
        Only need to specify list of quantity value
        :return:
        """
        options = []

        # quantity value
        qty_list = []
        with h5py.File(self.file_path, "r") as f:
            keys = [s for s in f["dataset1"].keys() if "data" in s]
            for i in range(len(keys)):
                k = keys[i]
                qty = f["dataset1"][k]["what"].attrs["quantity"]
                qty = qty[0].decode("utf-8")  # was array of b string
                t = (qty, k)
                qty_list.append(t)
        options.append({
            "key": "qty",
            "type": "dropdown",
            "options": qty_list,
            "description": "Quantity"
        })

        return options

    def get_profile(self, config):
        """Return the profile string
                :return:
                """
        self.process(config)
        c = copy.deepcopy(config)
        qty = c["options"]["qty"]
        with h5py.File(self.file_path, "r") as f:
            qty = f["dataset1"][qty]["what"].attrs["quantity"]
            if isinstance(qty, np.ndarray): qty = qty[0]
            qty = qty.decode("utf-8")
        c["options"] = {
            "Quantity": qty,
            "Colormap": ("hot_r", (self.v_min, self.v_max), "log"),
            "Bounds": self.bounds,
            "Center": self.center
        }
        return c


class Opera:
    """
    Process raster data with grid in polar coordinate system.
    """

    def __init__(self, file_path, config, temp_set_dir):
        self.file_path = file_path
        self.data = None
        self.bounds = None
        self.center = None
        self.v_min = 0
        self.v_max = 500
        self.dt = None
        self.grid = None

        # directly start processing
        if config is not None and temp_set_dir is not None:
            self.process(config=config)
            self.create_temp_image(temp_set_dir=temp_set_dir)
            self.create_temp_data_file(temp_set_dir=temp_set_dir)

    def process(self, config):
        """
        Get all the attributes and transform coordinate system from
        spherical to geographical. Config of PolarVol2D should include
        information of scan and qty.
        :return:
        """
        # Read the data and all the necessary attributes
        scan = config["options"]["scan"]
        qty = config["options"]["qty"]
        with h5py.File(self.file_path, "r") as f:
            self.data = f[scan][qty]["data"][...]

            # prepare attributes
            elangle = f[scan]["where"].attrs["elangle"]
            if isinstance(elangle, np.ndarray): elangle = elangle[0]
            elangle = float(elangle)

            rscale = f[scan]["where"].attrs["rscale"]
            if isinstance(rscale, np.ndarray): rscale = rscale[0]
            rscale = float(rscale)

            nbins = f[scan]["where"].attrs["nbins"]
            if isinstance(nbins, np.ndarray): nbins = nbins[0]
            nbins = int(nbins)

            nrays = f[scan]["where"].attrs["nrays"]
            if isinstance(nrays, np.ndarray): nrays = nrays[0]
            nrays = int(nrays)

            gain = f[scan][qty]["what"].attrs["gain"]
            if isinstance(gain, np.ndarray): gain = gain[0]
            gain = float(gain)

            offset = f[scan][qty]["what"].attrs["offset"]
            if isinstance(offset, np.ndarray): offset = offset[0]
            offset = float(offset)

            nodata = f[scan][qty]["what"].attrs["nodata"]
            if isinstance(nodata, np.ndarray): nodata = nodata[0]
            nodata = float(nodata)

            undetect = f[scan][qty]["what"].attrs["undetect"]
            if isinstance(undetect, np.ndarray): undetect = undetect[0]
            undetect = float(undetect)

            lon = f["where"].attrs["lon"]
            if isinstance(lon, np.ndarray): lon = lon[0]
            lon = float(lon)

            lat = f["where"].attrs["lat"]
            if isinstance(lat, np.ndarray): lat = lat[0]
            lat = float(lat)

            height = f["where"].attrs["height"]
            if isinstance(height, np.ndarray): height = height[0]
            height = float(height)

            date = f["what"].attrs["date"]
            if isinstance(date, np.ndarray): date = date[0]
            date = date.decode("utf-8")

            tp = f["what"].attrs["time"]
            if isinstance(tp, np.ndarray): tp = tp[0]
            tp = tp.decode("utf-8")

        # Mask nodata and undetect value with nan
        self.data = self.data.astype(float)
        self.data[self.data == undetect] = np.nan
        self.data[self.data == nodata] = np.nan

        # compute unit values
        self.data = self.data * gain + offset

        # Datetime stamp
        self.dt = parser.parse(date + " " + tp)
        self.dt = self.dt.replace(second=0, microsecond=0)  # ignore second
        self.dt = self.dt.strftime("%Y%m%d %H%M")  # transfer back to str

        # Compute v_min and v_max for choosing the boundaries of colormap
        self.v_min = np.nanmin(self.data)
        self.v_max = np.nanmax(self.data)
        if 0 <= self.v_min <= self.v_max <= 1:
            self.v_min = 0
            self.v_max = 1
        elif 0 <= self.v_min <= self.v_max <= 10:
            self.v_min = 0
            self.v_max = 10
        elif -50 <= self.v_min <= self.v_max <= 100:
            self.v_min = -50
            self.v_max = 100
        else:
            self.v_min = 0
            self.v_max = 350

        # Compute geometa (3857 grid, bbox, and center)
        self.grid = np.empty((nrays, nbins, 3))  # azimuth, range, height
        self.grid = wrl.georef.sweep_centroids(nrays=nrays, rscale=rscale,
                                               nbins=nbins, elangle=elangle)
        self.grid = np.insert(self.grid, 0, 0, axis=1)
        self.grid = np.insert(self.grid, nrays, self.grid[0, :, :], axis=0)

        grid_4326 = wrl.georef.polar.spherical_to_proj(
            self.grid[..., 0], self.grid[..., 1], self.grid[..., 2],
            (lon, lat, height),  # site coordinates
        )

        self.grid = wrl.georef.polar.spherical_to_proj(
            self.grid[..., 0], self.grid[..., 1], self.grid[..., 2],
            (lon, lat, height),  # site coordinates
            proj=wrl.georef.projection.epsg_to_osr(3857),
        )
        np.set_printoptions(linewidth=np.inf)

        self.bounds = [
            [np.nanmin(grid_4326[..., 1]), np.nanmin(grid_4326[..., 0])],
            [np.nanmax(grid_4326[..., 1]), np.nanmax(grid_4326[..., 0])],
        ]
        self.center = (lat, lon)

    def create_temp_data_file(self, temp_set_dir=None):
        """
        Temp data file keeps the cell values only. This is for filtering and
        masking in views.
        :param temp_set_dir:
        :return:
        """
        temp_data_dir = os.path.join(temp_set_dir, "data")

        # save as .h5 with compression_opts = 9 (output 1.4 MB per file)
        temp_data_path = os.path.join(temp_data_dir, self.dt + ".h5")
        with h5py.File(temp_data_path, "w") as f:
            f.create_dataset("data", data=self.data,
                             compression="gzip", compression_opts=9)

    def create_temp_image(self, temp_set_dir=None):
        """
        Create temp file that is the raster image.
        :return:
        """
        temp_image_dir = os.path.join(temp_set_dir, "images")
        temp_image_path = os.path.join(temp_image_dir, self.dt + ".png")

        r = self.data.shape[1] * 2
        plt.figure(figsize=(r, r), dpi=1)
        x_range = [np.nanmin(self.grid[..., 0]), np.nanmax(self.grid[..., 0])]
        y_range = [np.nanmin(self.grid[..., 1]), np.nanmax(self.grid[..., 1])]
        plt.xlim(x_range)
        plt.ylim(y_range)
        plt.pcolormesh(self.grid[..., 0], self.grid[..., 1], self.data,
                       cmap="jet", vmin=self.v_min, vmax=self.v_max, snap=True,
                       )
        plt.axis("off")
        plt.savefig(temp_image_path, transparent=True, bbox_inches="tight",
                    pad_inches=0, dpi=1)
        plt.close()

    def get_options(self):
        o_list = []

        # Scan
        scan_list = []
        with h5py.File(self.file_path, "r") as f:
            keys = [s for s in f.keys() if "dataset" in s]
            for i in range(len(keys)):
                k = keys[i]
                elangle = f[k]["where"].attrs["elangle"]
                t = ("Scan " + str(i) + " (Elev. = " + str(elangle) + ")", k)
                scan_list.append(t)
        o_list.append({
            "key": "scan",
            "type": "dropdown",
            "options": scan_list,
            "description": "Scan"
        })

        # Quantity
        qty_list = []
        with h5py.File(self.file_path, "r") as f:
            keys = [s for s in f["dataset1"].keys() if "data" in s]
            for i in range(len(keys)):
                k = keys[i]
                qty = f["dataset1"][k]["what"].attrs["quantity"].decode("utf-8")
                t = (qty, k)
                qty_list.append(t)
        o_list.append({
            "key": "qty",
            "type": "dropdown",
            "options": qty_list,
            "description": "Quantity"
        })

        return o_list

    def get_profile(self, config):
        """Return the profile string
        :return:
        """
        self.process(config)
        c = copy.deepcopy(config)
        scan = c["options"]["scan"]
        qty = c["options"]["qty"]
        with h5py.File(self.file_path, "r") as f:
            scan = "Elev. = " + str(f[scan]["where"].attrs["elangle"])
            qty = f["dataset1"][qty]["what"].attrs["quantity"].decode("utf-8")
        c["options"] = {
            "Scan": scan,
            "Quantity": qty,
            "Colormap": ("jet", (self.v_min, self.v_max), "linear"),
            "Bounds": self.bounds,
            "Center": self.center
        }
        return c


class KNMIH5:
    def __init__(self):
        pass
