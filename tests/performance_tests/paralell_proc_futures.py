#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 13:35:00 2020

@author: Berend-Christiaan Wijers
"""

# %% Libraries
import multiprocessing as mp  # Multiprocessing
import test_calculations as tc
import time
import logging.config # Logging module
import os
import json
from concurrent.futures import ProcessPoolExecutor  # Multiprocessing
import psutil
import platform
import distro
import sys

# %% Functions
def cpu_info():
    if platform.system() == 'Windows':
        return platform.processor()
    elif platform.system() == 'Darwin':
        command = '/usr/sbin/sysctl -n machdep.cpu.brand_string'
        return os.popen(command).read().strip()
    elif platform.system() == 'Linux':
        command = 'cat /proc/cpuinfo'
        return os.popen(command).read().strip()
    return 'platform not identified'

def setup_logging(
    default_path='../conf/logging.json',
    default_level=logging.INFO,
    env_key='LOG_CFG'
                ):

    """Setup logging configuration"""
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path, 'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)

def linux_distribution():

    try:
        return " ".join(list(distro.linux_distribution()))
    except:
        return "N/A"

def system_info():
    #platform.mac_ver(),
    #mac_ver: %s
    #platform.uname(),
    #platform: %s

    return print("""
Python version: %s
linux_distribution: %s
system: %s
machine: %s
platform: %s
version: %s
CPU Cores: %s
CPU Freq Min [MHz]: %s
CPU Freq Max [MHz]: %s
Total Memory [GB]: %s
""" %(
    " ".join(sys.version.split('\n')),
    linux_distribution(),
    platform.system(),
    platform.machine(),
    platform.platform(),
    platform.version(),
    mp.cpu_count(),
    psutil.cpu_freq().min,
    psutil.cpu_freq().max,
    str( round(psutil.virtual_memory().total / (1024 * 1024 * 1024),2) )
    ))

# %% Env var
test_length = 100000 # 100.000
repeat = 5
setup_logging()
# %% Main

# %% Multiplication test
if __name__ == "__main__":
    logger = logging.getLogger(__name__)
    #logger.setLevel("")


    # Metrics init
    elapsed_time_list = []
    avg_cpu_usage_list = []
    avg_mem_usage_list = []
    cpu_usage = psutil.cpu_percent()
    #p = psutil.Process(os.getpid())

    for i in range(repeat):
        start_time = time.time()
        logger.info("Started testing the performance of Parallel processing with ProcessPoolExecutor")
        with ProcessPoolExecutor(max_workers=mp.cpu_count()) as executor:
            futures = []
            for i in range(test_length):
                futures.append(executor.submit(tc.multiplication, i))

        for future in futures:
            future.result()

        end_time = time.time()

        # Metrics
        elapsed_time_list.append(end_time - start_time)
        cpu_percent = psutil.cpu_percent(percpu=True)
        cpu_percent = [str(p) for p in cpu_percent]
        avg_cpu_usage_list.append(" ".join(cpu_percent))
        avg_mem_usage_list.append(psutil.virtual_memory().percent)

    logger.info("Finished testing with n_repeats: {} and val range: {}".format(repeat,test_length))
    # Gather system information
    logger.info(system_info())
    logger.info(elapsed_time_list)
    logger.info(avg_cpu_usage_list)
    logger.info(avg_mem_usage_list)






