from .temp import *
from .task import *
from .view import *
from .toolkit import *
from .plugin import *
import warnings

if __import__("ipymeteovis"):
    # make folder of tempset, output and download if not exists
    if not os.path.exists(TEMP_SETS_DIR):
        os.mkdir(TEMP_SETS_DIR)
    if not os.path.exists(OUTPUT_DIR):
        os.mkdir(OUTPUT_DIR)
    if not os.path.exists(DOWNLOAD_DIR):
        os.mkdir(DOWNLOAD_DIR)

    # ignore warning messages
    warnings.filterwarnings("ignore")
