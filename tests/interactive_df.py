#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 15:58:32 2020

@author: bwijers1
"""

from minio import Minio
import pandas as pd
import qgrid
import getpass


def widget_df(df):
    """
	Aim:Any pandas dataframe to a widget DF.
	Status:Seems to not work correctly yet
	"""
    qgrid_widget = qgrid.show_grid(df, show_toolbar=True)
    qgrid_widget
    return df


def s3_df():
    """
    Wrapper function to handle the full pipeline for retrieving and displaying results
    from S3
    Aim: Starting point for a user to retrieve a DF with information about remote stored objects.
    Status: working
    """
    minioClient = prompt_s3_connection_details()
    objects = minio_list_objects(minioClient)
    df = objects_to_df(objects)
    widget_df(df)
    return df


def prompt_s3_connection_details():
    """
    Function to be called to use a s3 source
    For now it is our own Minio server for which we will fill in the defaults
    We can call this function which will set everything up for us and call relevant functions.
    We can also call each function independantly such that we have more controll.
    """
    s3_access = input('Please enter your s3 access key: \n')
    s3_secret = getpass.getpass('Please enter your s3 secret key: \n')
    # For s3 amazon in a future stage
    s3_endpoint = "fnwi-s0.science.uva.nl:9001"
    # s3_endpoint = input('Please enter your s3 endpoint key: \n')
    # s3_secure = input("Please enter if this s3 connection should be secure: \n")
    # s3_secure = bool(s3_secure)

    minioClient = Minio(endpoint=s3_endpoint,
                        access_key=s3_access,
                        secret_key=s3_secret,
                        secure=True
                        )
    return minioClient


def minio_list_objects(minioClient,
                       bucket="rbc", prefix="/"):
    """
    List objects
    IN:
        minioClient : initialized minioclient object
        bucket : str
        prefix : str
    RETURN:
        generator
     as a pandas DF
    """
    objects = minioClient.list_objects(bucket_name=bucket,
                                       prefix=prefix,  # /path/to/files
                                       recursive=True)

    return objects


def objects_to_df(objects, dt_to_index=False):
    df = pd.DataFrame([vars(f) for f in objects])
    if dt_to_index:
        raise NotImplementedError("Date time to index has not been implemented")
    return df


def rbc_df(df):
    """
    Transforms the object DF to a more useable one for users.
    Cols:
    Country, Radar, Date
    """
    # Grab all object, split and create a new DF.
    # Split a prefix:
    # -Country, Radar, Year, Month, Day, timestamp (datetime), Fname, ??
